# Products API

This Products API provides CRUD functionality for `Products` and `ProductOptions`. The api is written on asp net core 2.2 while the tests are written in xUnit. The data is written to a local SQLite database which cna be found in `/AppData/products.db`

## How to run the API

### Via an IDE (like Visual Studio)
* Open the solution via an IDE
* Select the `ProductsApi` as the startup project and click `Run`
* This should open a browser page with endpoints displayed via Swagger.
If it does not, it can be manually triggered by navigating to `http://localhost:5001/swagger/index.html`

### Via terminal
* Open a terminal of our choice (Powershell / Git bash / cmd etc)
* Navigate to the project directory where `ProductsApi.csproj` file resides
* Type `dotnet run` and press `Enter`.
* Open a browser and navigate to `http://localhost:5001/swagger/index.html` to access Swagger with endpoints

## How to run the unit tests

### Via an IDE (like Visual Studio)
* Open the solution via an IDE
* `Build` the solution so the tests are identified
* Navigate to the `Test Explorer` window -> Run all tests to see them pass

### Via terminal
* Open a terminal of your choice
* Navigate to the project directory where `ProductsApi.Test.csproj` file resides
* Type `dotnet test` and press `Enter` -> This runs all unit tests and shows results

## Endpoints / Swagger

This project uses Swagger to document the API. It can be accessed by navigating to `http://localhost:5001/swagger/index.html` while the project is running. Following are the endpoints for this API:

### Products
1. `GET api/products` - gets all products.
2. `GET api/products/{id}` - gets the project that matches the specified ID - ID is a GUID.
3. `GET api/products/name/{name}` - finds all products matching the specified name.
4. `POST api/products` - creates a new product.
5. `PUT api/products/{id}` - updates a product.
6. `DELETE api/products/{id}` - deletes a product and its options.

### ProductOptions
7. `GET api/products/{productId}/options` - finds all options for a specified product.
8. `GET api/products/{productId}/options/{id}` - finds the specified product option for the specified product.
9. `POST api/products/{productId}/options` - adds a new product option to the specified product.
10. `PUT api/products/{productId}/options/{id}` - updates the specified product option.
11. `DELETE api/products/{productId}/options/{id}` - deletes the specified product option.

## Database
The SQLite database with test data resides in `/AppData` folder, titled `products.db`. All manipulation of data when running the project will affect this db.

Use a tool like `DB Browser for SQLite` to browse data visually

### Changes made to the database from original
* Added primary key, foreign key and indexing to the tables based on the structure.
* `Collation` has been set directly at the table level for both the tables with `COLLATE NOCASE` as the value.

## Miscellaneous

* Basic model validation is taken care of by `ApiController` attribute to controllers.
* Additional validations are performed by `ServiceFilters` to specific endpoints.
* Exceptions are handled by `Exception Filter`, implemented via exception helpers following  `Chain of responsibility` pattern.
* The API has been designed based on `Anaemic Domain Model` pattern with `Models` having no logic in them while `Services` taking care of logic instead.