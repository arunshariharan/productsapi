﻿using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Interfaces
{
    public interface IProductOptionsService
    {
        Task<ProductOptions> GetAllProductOptions(Guid productId);
        Task<ProductOption> GetProductOption(Guid productId, Guid id);
        Task<(Guid, Guid)> AddProductOption(Guid productId, ProductOption option);
        Task UpdateProductOption(Guid productId, Guid id, ProductOption option);
        Task DeleteProductOption(Guid productId, Guid id);
    }
}