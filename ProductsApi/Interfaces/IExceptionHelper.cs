﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Interfaces
{
    public interface IExceptionHelper
    {
        string RegisterException(ref ExceptionContext context);
    }
}
