﻿using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Interfaces
{
    public interface IProductService
    {
        Task<Products> GetProducts();
        Task<Product> GetProductById(Guid id);
        Task<Products> GetProductsByName(string name);
        Task<Guid> AddProduct(Product product);
        Task DeleteProduct(Guid id);
        Task UpdateProduct(Guid id, Product product);
    }
}
