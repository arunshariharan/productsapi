﻿using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Interfaces
{
    public interface IProductOptionsRepository
    {
        Task<List<ProductOption>> GetAllProductOptionsAsync(Guid productId);
        Task<ProductOption> GetProductOptionAsync(Guid productId, Guid id);
        Task AddProductOptionAsync(ProductOption option);
        Task<bool> ProductOptionExistsAsync(Guid productId, Guid id);
        Task UpdateProductOptionAsync(ProductOption option);
        Task DeleteProductOptionAsync(Guid productId, Guid id);
    }
}
