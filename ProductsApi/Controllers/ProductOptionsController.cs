﻿using Microsoft.AspNetCore.Mvc;
using ProductsApi.Filters;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Controllers
{
    [Route("api/products/{productId}/options")]
    [ApiController]
    public class ProductOptionsController : ControllerBase
    {
        private readonly IProductOptionsService _productOptionsService;

        public ProductOptionsController(IProductOptionsService productOptionsService)
        {
            _productOptionsService = productOptionsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(Guid productId)
        {
            var result = await _productOptionsService.GetAllProductOptions(productId);
            return Ok(result);
        }

        [HttpGet("{id}")]
        [ServiceFilter(typeof(OptionExistsAsyncFilter))]
        public async Task<IActionResult> GetOption(Guid productId, Guid id)
        {
            var result = await _productOptionsService.GetProductOption(productId, id);
            return Ok(result);
        }

        [HttpPost]
        [ServiceFilter(typeof(ProductExistsAsyncFilter))]
        public async Task<IActionResult> AddOption(Guid productId, ProductOption option)
        {
            (Guid resultProductId, Guid resultId) = await _productOptionsService.AddProductOption(productId, option);
            return CreatedAtAction(nameof(GetOption),
                new { productId = resultProductId, id = resultId },
                new { ProductId = resultProductId, Id = resultId });
        }

        [HttpPut("{id}")]
        [ServiceFilter(typeof(OptionExistsAsyncFilter))]
        public async Task<IActionResult> UpdateOption(Guid productId, Guid id, ProductOption option)
        {
            await _productOptionsService.UpdateProductOption(productId, id, option);
            return Ok();
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(OptionExistsAsyncFilter))]
        public async Task<IActionResult> DeleteOption(Guid productId, Guid id)
        {
            await _productOptionsService.DeleteProductOption(productId, id);
            return Ok();
        }
    }
}
