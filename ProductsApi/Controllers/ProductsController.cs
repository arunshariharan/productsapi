﻿using Microsoft.AspNetCore.Mvc;
using ProductsApi.Filters;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using System;
using System.Threading.Tasks;

namespace ProductsApi.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var products = await _productService.GetProducts();
            return Ok(products);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var product = await _productService.GetProductById(id);
            return Ok(product);
        }

        [HttpGet("name/{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            var products = await _productService.GetProductsByName(name);
            return Ok(products);
        }

        [HttpPost]
        public async Task<IActionResult> Add(Product product)
        {
            var resultProductId = await _productService.AddProduct(product);
            return CreatedAtAction(nameof(GetById), new { id = resultProductId }, product);
        }

        [HttpPut("{id}")]
        [ServiceFilter(typeof(ProductExistsAsyncFilter))]
        public async Task<IActionResult> Update(Guid id, Product product)
        {
            await _productService.UpdateProduct(id, product);
            return Ok();
        }

        [HttpDelete("{id}")]
        [ServiceFilter(typeof(ProductExistsAsyncFilter))]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _productService.DeleteProduct(id);
            return Ok();
        }
    }
}

