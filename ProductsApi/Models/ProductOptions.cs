﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Models
{
    public class ProductOptions
    {
        public List<ProductOption> Items { get; set; }
    }
}
