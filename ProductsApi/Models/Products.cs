﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Models
{
    public class Products
    {
        public List<Product> Items { get; set; }
    }
}
