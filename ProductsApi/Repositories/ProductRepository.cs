﻿using ProductsApi.Interfaces;
using ProductsApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System;
using Microsoft.Extensions.Logging;

namespace ProductsApi.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IDbConnectionFactory _connectionFactory;
        private readonly ILogger<ProductRepository> _logger;

        public ProductRepository(IDbConnectionFactory connectionFactory, ILogger<ProductRepository> logger)
        {
            _connectionFactory = connectionFactory;
            _logger = logger;
        }

        public async Task AddProductAsync(Product product)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"INSERT INTO Products(Id, Name, Description, Price, DeliveryPrice)
                            VALUES(@Id, @Name, @Description, @Price, @DeliveryPrice)";
                await conn.ExecuteAsync(query, product);
            }
        }

        public async Task DeleteProductAsync(Guid id)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = "DELETE FROM Products WHERE Id = @Id";
                await conn.ExecuteAsync(query, new { Id = id });
            }
        }

        public async Task<List<Product>> GetAllProductsAsync()
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"SELECT Id, 
                            CAST(Price AS REAL) AS Price, 
                            CAST(DeliveryPrice AS REAL) AS DeliveryPrice, 
                            Name, 
                            Description 
                            FROM Products";
                var result = await conn.QueryAsync<Product>(query);
                return result.ToList();
            }            
        }

        public async Task<Product> GetProductByIdAsync(Guid id)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"SELECT Id, 
                            CAST(Price AS REAL) AS Price, 
                            CAST(DeliveryPrice AS REAL) AS DeliveryPrice, 
                            Name, 
                            Description 
                            FROM Products 
                            WHERE Id = @Id";
                var result =  await conn.QueryFirstOrDefaultAsync<Product>(query, new { Id = id });
                return result;
            }
        }

        public async Task<List<Product>> GetProductsByNameAsync(string name)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"SELECT Id, 
                            CAST(Price AS REAL) AS Price, 
                            CAST(DeliveryPrice AS REAL) AS DeliveryPrice, 
                            Name, 
                            Description 
                            FROM Products 
                            WHERE Name LIKE @Name";
                var result = await conn.QueryAsync<Product>(query, new { Name = $"%{name}%" });
                return result.ToList();
            }
        }

        public async Task<bool> ProductExistsAsync(Guid id)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = "SELECT 1 FROM Products WHERE Id = @Id";
                var result = await conn.QueryAsync(query, new { Id = id });
                return result.Count() > 0;
            }
        }

        public async Task UpdateProductAsync(Product product)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"UPDATE Products 
                            SET Id = @Id, Name = @Name, Price = @Price, Description = @Description, DeliveryPrice = @DeliveryPrice
                            WHERE Id = @Id";
                await conn.ExecuteAsync(query, product);
            }
        }
    }
}
