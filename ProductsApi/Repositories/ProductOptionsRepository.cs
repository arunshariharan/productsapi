﻿using Dapper;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Repositories
{
    public class ProductOptionsRepository : IProductOptionsRepository
    {
        private readonly IDbConnectionFactory _connectionFactory;
        public ProductOptionsRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _connectionFactory = dbConnectionFactory;
        }

        public async Task AddProductOptionAsync(ProductOption option)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"INSERT INTO ProductOptions (Id, ProductId, Name, Description)
                            VALUES(@Id, @ProductId, @Name, @Description)";
                await conn.ExecuteAsync(query, option);
            }
        }

        public async Task DeleteProductOptionAsync(Guid productId, Guid id)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = "DELETE FROM ProductOptions WHERE ProductID = @ProductId AND Id =  @Id";
                await conn.ExecuteAsync(query, new { ProductId = productId, Id = id });
            }
        }

        public async Task<List<ProductOption>> GetAllProductOptionsAsync(Guid productId)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = "SELECT * FROM ProductOptions WHERE ProductId = @ProductId";
                var result = await conn.QueryAsync<ProductOption>(query, new { ProductId = productId });
                return result.ToList();
            }
        }

        public async Task<ProductOption> GetProductOptionAsync(Guid productId, Guid id)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = "SELECT * FROM ProductOptions WHERE ProductID = @ProductId AND Id = @Id";
                return await conn.QueryFirstOrDefaultAsync<ProductOption>(query, new { ProductId = productId, Id = id });
            }
        }

        public async Task<bool> ProductOptionExistsAsync(Guid productId, Guid id)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = "SELECT 1 FROM ProductOptions WHERE ProductId = @ProductId AND Id = @Id";
                var result = await conn.QueryAsync(query, new { ProductId = productId, Id = id });
                return result.Count() > 0;
            }
        }

        public async  Task UpdateProductOptionAsync(ProductOption option)
        {
            using (var conn = _connectionFactory.CreateConnection())
            {
                var query = @"UPDATE ProductOptions 
                            SET Id = @Id, ProductId = @ProductId, Name = @Name, Description = @Description
                            WHERE Id = @Id";
                await conn.ExecuteAsync(query, option);
            }
        }
    }
}
