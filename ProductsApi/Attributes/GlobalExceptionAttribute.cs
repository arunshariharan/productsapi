﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProductsApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Attributes
{
    public class GlobalExceptionAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<GlobalExceptionAttribute> _logger;
        private readonly IEnumerable<IExceptionHelper> _helpers;

        public GlobalExceptionAttribute(IEnumerable<IExceptionHelper> helpers, ILogger<GlobalExceptionAttribute> logger)
        {
            _logger = logger;
            _helpers = helpers;
        }

        public override void OnException(ExceptionContext context)
        {
            string error = "";

            foreach (var exceptionHelper in _helpers)
            {
                error = exceptionHelper.RegisterException(ref context);
            }

            if (string.IsNullOrEmpty(error))
            {
                error = JsonConvert.SerializeObject(new { Error = context.Exception.Message });

                _logger.LogError($"Request for: {context.HttpContext.Request.Path} , " +
                   $"Method: {context.HttpContext.Request.Method} , " +
                   $"Queries: {context.HttpContext.Request.QueryString} , " +
                   $"Error: {context.Exception.Message} , " +
                   $"Unhandled Exception occured: {context.Exception.StackTrace}");

                context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }

            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.WriteAsync(error);
            context.ExceptionHandled = true;
        }

    }
}
