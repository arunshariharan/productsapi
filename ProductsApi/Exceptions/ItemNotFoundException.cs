﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public ProductNotFoundException(string errorMessage) : base(errorMessage)
        {

        }
    }
}
