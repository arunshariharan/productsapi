﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProductsApi.Exceptions;
using ProductsApi.Interfaces;
using System;

namespace ProductsApi.Helpers
{
    public class ProductNotFoundExceptionHelper : IExceptionHelper
    {
        private readonly ILogger<ProductNotFoundExceptionHelper> _logger;

        public ProductNotFoundExceptionHelper(ILogger<ProductNotFoundExceptionHelper> logger)
        {
            _logger = logger;
        }

        public string RegisterException(ref ExceptionContext context)
        {
            if (context.Exception.GetType() != typeof(ProductNotFoundException))
            {
                return string.Empty;
            }

            var exception = (ProductNotFoundException)context.Exception;
            string error = JsonConvert.SerializeObject(new
            {
                Error = exception.Message,
                Id = exception.Id,
            });

            _logger.LogError($"Id: {exception.Id}, Message: {exception.Message}, Unhandled Exception occured at : {exception.StackTrace}");

            context.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;

            return error;
        }
    }
}
