﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProductsApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Helpers
{
    public class ArgumentNullExceptionHelper : IExceptionHelper
    {
        private readonly ILogger<ArgumentNullExceptionHelper> _logger;

        public ArgumentNullExceptionHelper(ILogger<ArgumentNullExceptionHelper> logger)
        {
            _logger = logger;
        }

        public string RegisterException(ref ExceptionContext context)
        {
            if(context.Exception.GetType() != typeof(ArgumentNullException))
            {
                return string.Empty;
            }

            var exception = (ArgumentNullException)context.Exception;
            string error = JsonConvert.SerializeObject(new { Error = exception.Message });
            _logger.LogError($"{exception.Message}, Unhandled Exception occured at : {exception.StackTrace}");

            context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

            return error;
        }
    }
}
