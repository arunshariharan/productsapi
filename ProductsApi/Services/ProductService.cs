﻿using ProductsApi.Exceptions;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductsApi.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _repository;

        public ProductService(IProductRepository repositoryService)
        {
            _repository = repositoryService;
        }

        public async Task<Guid> AddProduct(Product product)
        {
            product.Id = (product.Id == Guid.Empty) ? Guid.NewGuid() : product.Id;
            await _repository.AddProductAsync(product);
            return product.Id;
        }

        public async Task<Product> GetProductById(Guid id)
        {
            var product = await _repository.GetProductByIdAsync(id);

            if (product is null)
            {
                throw new ProductNotFoundException("No such product found") { Id = id };
            }

            return product;
        }

        public async Task<Products> GetProductsByName(string name)
        {
            var productList = await _repository.GetProductsByNameAsync(name);

            if (productList is null || productList.Count == 0)
            {
                throw new ProductNotFoundException("No such product found") { Name = name };
            }

            return new Products { Items = productList };
        }

        public async Task<Products> GetProducts()
        {
            var productList = await _repository.GetAllProductsAsync();
            return new Products { Items = productList };
        }

        public async Task UpdateProduct(Guid id, Product product)
        { 
            product.Id = id;
            await _repository.UpdateProductAsync(product);
        }

        public async Task DeleteProduct(Guid id) => await _repository.DeleteProductAsync(id);
    }
}
