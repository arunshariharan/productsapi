﻿using ProductsApi.Exceptions;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Services
{
    public class ProductOptionsService : IProductOptionsService
    {
        private IProductOptionsRepository _repository;

        public ProductOptionsService(IProductOptionsRepository productOptionsRepository)
        {
            _repository = productOptionsRepository;
        }

        public async Task<(Guid, Guid)> AddProductOption(Guid productId, ProductOption option)
        {
            option.Id = (option.Id == Guid.Empty) ? Guid.NewGuid() : option.Id;
            option.ProductId = productId;

            await _repository.AddProductOptionAsync(option);

            return (option.ProductId, option.Id);
        }

        public async Task<ProductOptions> GetAllProductOptions(Guid productId)
        {
            var productOptionList = await _repository.GetAllProductOptionsAsync(productId);
            return new ProductOptions { Items = productOptionList };
        }

        public async Task<ProductOption> GetProductOption(Guid productId, Guid id)
        {
            var productOption = await _repository.GetProductOptionAsync(productId, id);
            if(productOption is null)
            {
                throw new ProductNotFoundException("No Product Option found") { Id = id };
            }

            return productOption;
        }

        public async Task UpdateProductOption(Guid productId, Guid id, ProductOption option)
        {
            option.Id = id;
            option.ProductId = productId;

            await _repository.UpdateProductOptionAsync(option);
        }

        public async Task DeleteProductOption(Guid productId, Guid id) 
            => await _repository.DeleteProductOptionAsync(productId, id);
    }
}
