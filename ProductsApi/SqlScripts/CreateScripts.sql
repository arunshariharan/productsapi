/* 
Create a temp table with Products, drop the original one to recreate with COLLATE NOCASE, Primary key and index 
*/

CREATE TABLE Products01 (Id, Name, Description, Price, DeliveryPrice);
INSERT INTO Products01 Select Id, Name, Description, Price, DeliveryPrice from Products 
DROP TABLE Products
CREATE TABLE "Products" (
	"Id"	varchar(36) NOT NULL DEFAULT NULL UNIQUE COLLATE NOCASE,
	"Name"	varchar(17) DEFAULT NULL,
	"Description"	varchar(35) DEFAULT NULL,
	"Price"	decimal(6 , 2) DEFAULT NULL,
	"DeliveryPrice"	decimal(4 , 2) DEFAULT NULL,
	PRIMARY KEY("Id")
);

CREATE INDEX "ix_products_id" ON "Products" (
	"Id" 
);

INSERT INTO Products (Id, Name, Description, Price, DeliveryPrice)
    SELECT Id, Name, Description, Price, DeliveryPrice FROM Products01;
DROP TABLE Products01;


/*
Recreate ProductOptions table with Primary Key, Foreign Key, Index and Collat nocase for Ids at table level
*/


CREATE TABLE ProductOptions01 (Id, ProductId, Name, Description);
INSERT INTO ProductOptions01 Select Id, ProductId, Name, Description from ProductOptions
DROP TABLE ProductOptions
CREATE TABLE "ProductOptions" (
	"Id"	varchar(36) NOT NULL DEFAULT NULL UNIQUE COLLATE NOCASE,
	"ProductId"	varchar(36) NOT NULL DEFAULT NULL COLLATE NOCASE,
	"Name"	varchar(50) DEFAULT NULL,
	"Description"	varchar(100) DEFAULT NULL,
	FOREIGN KEY("ProductId") REFERENCES "Products"("Id") ON DELETE CASCADE,
	PRIMARY KEY("Id")
);

CREATE INDEX "ix_productoptions_id_productid" ON "ProductOptions" (
	"Id",
	"ProductId"
);

INSERT INTO ProductOptions (Id, ProductId, Name, Description)
    SELECT Id, ProductId, Name, Description FROM ProductOptions01;
	
DROP TABLE ProductOptions01;