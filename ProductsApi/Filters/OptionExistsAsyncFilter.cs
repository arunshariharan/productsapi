﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProductsApi.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProductsApi.Filters
{
    public class OptionExistsAsyncFilter : IAsyncActionFilter
    {
        private readonly IProductOptionsRepository _repository;

        public OptionExistsAsyncFilter(IProductOptionsRepository repository)
        {
            _repository = repository;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            context.ActionArguments.TryGetValue("productId", out object productId);
            context.ActionArguments.TryGetValue("id", out object id);
            var product = await _repository.ProductOptionExistsAsync((Guid) productId, (Guid) id);

            if(!product)
            {
                context.Result = new BadRequestObjectResult($"No options found for the given product Id: {id} and ProductID {productId} combination");
            }
            else
            {
                await next();
            }
        }
    }
}
