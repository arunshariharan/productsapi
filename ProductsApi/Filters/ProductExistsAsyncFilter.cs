﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProductsApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsApi.Filters
{
    public class ProductExistsAsyncFilter : IAsyncActionFilter
    {
        private readonly IProductRepository _repository;

        public ProductExistsAsyncFilter(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            object productId;
            context.ActionArguments.TryGetValue("productId", out productId);

            if (productId is null)
                context.ActionArguments.TryGetValue("id", out productId);            

            var productPresence = await _repository.ProductExistsAsync((Guid)productId);

            if (!productPresence)
            {
                context.Result = new BadRequestObjectResult($"No such product found: {productId}. Create a product first before adding a product option");
            }
            else
            {
                await next();
            }
        }
    }
}
