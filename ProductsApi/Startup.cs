﻿using Dapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting.Internal;
using Newtonsoft.Json.Serialization;
using ProductsApi.Attributes;
using ProductsApi.Filters;
using ProductsApi.Handlers;
using ProductsApi.Helpers;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using ProductsApi.Repositories;
using ProductsApi.Services;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace ProductsApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private IHostingEnvironment _env { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(GlobalExceptionAttribute));
            })
            .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Products Api",
                    Description = "Code Challenge Refactored Api"
                });
            });

            services
                .AddSingleton<IProductService, ProductService>()
                .AddSingleton<IProductOptionsService, ProductOptionsService>()

                .AddTransient<IDbConnectionFactory>(x =>
                    new SqliteConnectionFactory(Configuration.GetConnectionString("Sqlite")))

                .AddSingleton<IProductRepository, ProductRepository>()
                .AddSingleton<IProductOptionsRepository, ProductOptionsRepository>()

                .AddSingleton<OptionExistsAsyncFilter>()
                .AddSingleton< ProductExistsAsyncFilter>()

                .AddSingleton<IExceptionHelper, ArgumentNullExceptionHelper>()
                .AddSingleton<IExceptionHelper, ProductNotFoundExceptionHelper>()
            ;

            // Use type handler to parse string <=> Guid in sqlite for dapper 
            SqlMapper.AddTypeHandler(new GuidTypeHandler());
            SqlMapper.RemoveTypeMap(typeof(Guid));
            SqlMapper.RemoveTypeMap(typeof(Guid?));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(ui =>
            {
                ui.SwaggerEndpoint("/swagger/v1/swagger.json", "Products Api v1");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
