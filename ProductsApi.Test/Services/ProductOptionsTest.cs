﻿using Moq;
using ProductsApi.Exceptions;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using ProductsApi.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProductsApi.Test.Services
{
    public class ProductOptionsTest
    {
        private readonly Mock<Product> _mockProduct;
        private readonly Mock<ProductOption> _mockProductOption;
        private readonly Mock<List<ProductOption>> _mockProductOptionList;
        private readonly Mock<IProductOptionsRepository> _mockProductRepository;

        private readonly ProductOptionsService _service;

        public ProductOptionsTest()
        {
            _mockProduct = new Mock<Product>();
            _mockProductOption = new Mock<ProductOption>();
            _mockProductOptionList = new Mock<List<ProductOption>>();
            _mockProductRepository = new Mock<IProductOptionsRepository>();
            _service = new ProductOptionsService(_mockProductRepository.Object);
        }

        [Fact]
        public async void GetAllProductOptions_should_return_productoptions_for_given_id()
        {
            _mockProductRepository.Setup(x => x.GetAllProductOptionsAsync(It.IsAny<Guid>())).ReturnsAsync(_mockProductOptionList.Object);
            var result = await _service.GetAllProductOptions(It.IsAny<Guid>());

            Assert.NotNull(result);
            Assert.IsType<ProductOptions>(result);
            Assert.Equal(_mockProductOptionList.Object, result.Items);
        }

        [Fact]
        public async void GetProductOption_should_return_product_option_when_present()
        {
            _mockProductRepository.Setup(x => x.GetProductOptionAsync(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync(_mockProductOption.Object);

            var result = await _service.GetProductOption(It.IsAny<Guid>(), It.IsAny<Guid>());

            Assert.NotNull(result);
            Assert.Equal(_mockProductOption.Object, result);
        }

        [Fact]
        public async void GetProductOption_should_return_404_when_id_is_absent()
        {
            _mockProductRepository.Setup(x => x.GetProductOptionAsync(It.IsAny<Guid>(), It.IsAny<Guid>())).ReturnsAsync((ProductOption) null);

            await Assert.ThrowsAsync<ProductNotFoundException>(async () => await _service.GetProductOption(It.IsAny<Guid>(), It.IsAny<Guid>()));
        }

        [Fact]
        public async void AddProductOption_should_return_existing_productid_when_present()
        {
            _mockProductOption.Object.Id = Guid.NewGuid();
            _mockProductRepository.Setup(x => x.AddProductOptionAsync(_mockProductOption.Object)).Returns(Task.CompletedTask);

            var (resultProductId, resultId) = await _service.AddProductOption(It.IsAny<Guid>(), _mockProductOption.Object);

            _mockProductRepository.Verify(x => x.AddProductOptionAsync(_mockProductOption.Object), Times.Once);
            Assert.Equal(_mockProductOption.Object.Id, resultId);
            Assert.Equal(_mockProductOption.Object.ProductId, resultProductId);
        }

        [Fact]
        public async void AddProductOption_should_return_new_productid_when_empty()
        {
            _mockProductOption.Object.Id = Guid.Empty;
            _mockProductRepository.Setup(x => x.AddProductOptionAsync(_mockProductOption.Object)).Returns(Task.CompletedTask);

            var result = await _service.AddProductOption(It.IsAny<Guid>(), _mockProductOption.Object);

            _mockProductRepository.Verify(x => x.AddProductOptionAsync(_mockProductOption.Object), Times.Once);
            Assert.NotEqual(_mockProductOption.Object.Id, Guid.Empty);
        }

        [Fact]
        public async void UpdateProductOption_shouldcall_repository_to_update_for_existing_product()
        {
            _mockProductRepository.Setup(x => x.UpdateProductOptionAsync(_mockProductOption.Object)).Returns(Task.CompletedTask);

            await _service.UpdateProductOption(It.IsAny<Guid>(), It.IsAny<Guid>(), _mockProductOption.Object);

            _mockProductRepository.Verify(x => x.UpdateProductOptionAsync(_mockProductOption.Object), Times.Once);
        }

        [Fact]
        public async void DeleteProductOption_should_call_repository_to_delete_product()
        {
            _mockProductRepository.Setup(x => x.DeleteProductOptionAsync(It.IsAny<Guid>(), It.IsAny<Guid>())).Returns(Task.CompletedTask);

            await _service.DeleteProductOption(It.IsAny<Guid>(),It.IsAny<Guid>());

            _mockProductRepository.Verify(x => x.DeleteProductOptionAsync(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Once);
        }
    }
}
