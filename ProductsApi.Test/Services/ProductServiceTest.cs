﻿using Moq;
using ProductsApi.Exceptions;
using ProductsApi.Interfaces;
using ProductsApi.Models;
using ProductsApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ProductsApi.Test.Services
{
    public class ProductServiceTest
    {
        private readonly Mock<IProductRepository> _mockProductRepository;
        private readonly ProductService _productService;
        private readonly Mock<List<Product>> _mockProductsList;
        private readonly Mock<Product> _mockProduct;

        public ProductServiceTest()
        {
            _mockProduct = new Mock<Product>();
            _mockProductsList = new Mock<List<Product>>();
            _mockProductRepository = new Mock<IProductRepository>(MockBehavior.Strict);
            _productService = new ProductService(_mockProductRepository.Object);
        }

        [Fact]
        public async void GetProducts_should_return_products_as_a_list()
        {
            _mockProductRepository.Setup(x => x.GetAllProductsAsync()).ReturnsAsync(_mockProductsList.Object);

            var result = await _productService.GetProducts();

            Assert.NotNull(result);
            Assert.IsType<Products>(result);
            Assert.Equal(_mockProductsList.Object, result.Items);
        }

        [Fact]
        public async void GetProductById_should_return_product_if_present()
        {
            _mockProductRepository.Setup(x => x.GetProductByIdAsync(It.IsAny<Guid>())).ReturnsAsync(_mockProduct.Object);

            var result = await _productService.GetProductById(It.IsAny<Guid>());

            Assert.NotNull(result);
            Assert.Equal(_mockProduct.Object, result);
        }

        [Fact]
        public async void GetProductById_should_throw_404Exception_if_product_absent()
        {
            _mockProductRepository.Setup(x => x.GetProductByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Product)null);

            await Assert.ThrowsAsync<ProductNotFoundException>(async () => await _productService.GetProductById(It.IsAny<Guid>()));
        }

        [Fact]
        public async void GetProductByName_should_return_product_if_present()
        {
            _mockProductRepository.Setup(x => x.GetProductsByNameAsync(It.IsAny<string>())).ReturnsAsync(new List<Product> { _mockProduct.Object });

            var result = await _productService.GetProductsByName(It.IsAny<string>());

            Assert.NotNull(result);
            Assert.IsType<Products>(result);
            Assert.Collection(result.Items, item => Assert.Equal(_mockProduct.Object, item));
        }

        [Fact]
        public async void GetProductByName_should_throw_404Exception_if_product_absent()
        {
            _mockProductRepository.Setup(x => x.GetProductsByNameAsync(It.IsAny<string>())).ReturnsAsync((List<Product>)null);  

            await Assert.ThrowsAsync<ProductNotFoundException>(async () => await _productService.GetProductsByName(It.IsAny<string>()));
        }

        [Fact]
        public async void AddProduct_should_return_existing_productid_when_present()
        {
            _mockProduct.Object.Id = Guid.NewGuid();
            _mockProductRepository.Setup(x => x.AddProductAsync(_mockProduct.Object)).Returns(Task.CompletedTask);

            var result = await _productService.AddProduct(_mockProduct.Object);

            _mockProductRepository.Verify(x => x.AddProductAsync(_mockProduct.Object), Times.Once);
            Assert.Equal(_mockProduct.Object.Id, result);
        }

        [Fact]
        public async void AddProduct_should_return_new_productid_when_empty()
        {
            _mockProduct.Object.Id = Guid.Empty;
            _mockProductRepository.Setup(x => x.AddProductAsync(_mockProduct.Object)).Returns(Task.CompletedTask);

            var result = await _productService.AddProduct(_mockProduct.Object);

            _mockProductRepository.Verify(x => x.AddProductAsync(_mockProduct.Object), Times.Once);
            Assert.NotEqual(_mockProduct.Object.Id, Guid.Empty);
        }

        [Fact]
        public async void UpdateProduct_shouldcall_repository_to_update_for_existing_product()
        {
            _mockProductRepository.Setup(x => x.UpdateProductAsync(_mockProduct.Object)).Returns(Task.CompletedTask);

            await _productService.UpdateProduct(It.IsAny<Guid>(), _mockProduct.Object);

            _mockProductRepository.Verify(x => x.UpdateProductAsync(_mockProduct.Object), Times.Once);
        }

        [Fact]
        public async void DeleteProduct_should_call_repository_to_delete_product_if_present()
        {
            _mockProductRepository.Setup(x => x.ProductExistsAsync(It.IsAny<Guid>())).ReturnsAsync(true);
            _mockProductRepository.Setup(x => x.DeleteProductAsync(It.IsAny<Guid>())).Returns(Task.CompletedTask);

            await _productService.DeleteProduct(It.IsAny<Guid>());

            _mockProductRepository.Verify(x => x.DeleteProductAsync(It.IsAny<Guid>()), Times.Once);
        }
    }
}
